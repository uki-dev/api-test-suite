// Public Packages
import { readFileSync, writeFileSync, copyFileSync } from 'fs';
import { } from 'mochawesome';

// Private Packages
// None

// Internal Modules
// None


const {
    BASE_URL,
    BITBUCKET_BRANCH,
    BITBUCKET_BUILD_NUMBER,
    BITBUCKET_GIT_HTTP_ORIGIN,
    BITBUCKET_REPO_FULL_NAME,
    KEYCLOAK_USERNAME,
    SERVICE_NAME,
    TEST_URL
} = process.env;

const colors = {
    success: 4437377, // #43b581
    warning: 15771405, // #f0a70d
    failure: 13565952 // #cf0000
};

interface Test {
    title: string;
    fullTitle: string;
    timedOut: boolean;
    duration: number;
    state: string;
    speed: string;
    pass: boolean;
    fail: boolean;
    pending: boolean;
    context: any;
    code: string;
    err?: {
        message: string;
        estack: string;
        diff: string;
    };
    uuid: string;
    parentUUID: string;
    isHook: boolean;
    skipped: boolean;
}

const getEmbed = (test: Test) => {
    let embed: any = {
        title: test.title,
        color: test.pass ? colors.success : colors.failure,
        footer: {
            text: `time: ${test.duration}s`
        }
    }

    if (test.err != null) {
        embed.description = '```\n' +
            test.err.message + '\n' +
            '```\n' +
            '```diff\n' +
            test.err.diff + '\n' +
            '```\n' +
            '```\n' +
            test.err.estack +
            '\n```';
    }

    return embed;
}

const flatlist = {};

const flatten = (node, collector = []) => {
    if (Array.isArray(node.tests)) {
        node.tests.forEach(test => {
            collector.push({ ...test, suiteName: node.title });
            flatlist[node.title] = flatlist[node.title] || [];
            flatlist[node.title].push(test);
        });
    }

    node.suites?.forEach(suite => flatten(suite, collector));
}

const result = JSON.parse(readFileSync(`./artifacts/test-results.json`, { encoding: 'utf-8' }));

result.results.forEach(x => {
    const suite = [];
    flatten(x, suite);
});

const embeds = [];
const resultChips = [];
for (let key in flatlist) {
    resultChips.push(`${key}: ${flatlist[key].map(x => x.fail ? ':x:' : ':white_check_mark:').join(' ')}${flatlist[key].filter(x => x.fail).length > 0 ? ' @here' : ''}`);
    const newEmbeds = flatlist[key].filter(x => x.fail).map(getEmbed);
    if (newEmbeds.length > 0) embeds.push(...newEmbeds);
}

const payload = {
    content: [
        `**Test summary**`,
        `Results available at ${TEST_URL}`,
        ...resultChips,
        `Environment: ${BASE_URL}`,
        `Execution: ${BITBUCKET_BUILD_NUMBER}`,
        `Test User: \`${KEYCLOAK_USERNAME}\``,
        'Report:'
    ].join('\n'),
    embeds: embeds
}

writeFileSync(`./artifacts/success_payload.json`, JSON.stringify(payload, null, 2));
copyFileSync(`./testplan/tests.json`, `./artifacts/testplan.json`);