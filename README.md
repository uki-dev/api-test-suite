# SuperTest-API-OpenDash360

This is a Javascript project that uses [Supertest](https://github.com/visionmedia/supertest), dotenv, Chai and Mocha for OD360 microservices automated API testing. It includes an example file for a GET request to opendash360.com/app/ test endpoint.

## Getting Started

```code
git clone git@bitbucket.org:uki-dev/supertest-api-opendash360.git
cd supertest-api-boilerplate
npm install
```

## dotenv

An example dotenv file is included and should be copied as `.env` in root. Enter valid credentials before running tests.

### `/example/test/example_test.js`

```javascript
require('dotenv').config();

const request = require('supertest');
const chai = require('chai');

const expect = chai.expect;

const baseUrl = process.env.BASE_URL;

const resource = {
    test: '/app/',
};

describe('API Example test', () => {

    it('should return a response with HTTP code 200', async () => {
        let response = await request(baseUrl)

            .get(resource.test)
            .then(response => {

                return response;
            });

        expect(response.status).to.equal(200);

        console.log('Actual Response: ' + response.status);
        console.log('Actual Response Body: ' + response.body);
        console.log('\n**RESPONSE STATUS** \n \n' + response.status);
        console.log('\n**RESPONSE TEXT** \n \n' + response.text);
    });
});
```

## Test example

The test in this project uses [supertest](https://github.com/visionmedia/supertest) to demonstrate how to send and receive requests and responses with some of OD360's microservices [official documentation](https://ukidev.atlassian.net/wiki/spaces/EPT/pages/76349469/API+Documentation).

Run example test with the following command:

```code
npm run test:example
```

## Reporters

[Mochawesome](https://github.com/adamgruber/mochawesome) and [mochawesome-report-generator](https://github.com/adamgruber/mochawesome-report-generator) are used in conjunction to generate an html report of your tests. This can be found in `/mochawesome-report/assets/mochawesome.html`. Reporter settings have been configured in the npm run script in package.json.

## CI/CD

//TODO
