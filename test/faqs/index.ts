// Public Packages
import { expect } from 'chai';
import { step } from 'mocha-steps';
import * as request from 'supertest'
import * as addContext from 'mochawesome/addContext';

// Private Packages
// None

// Internal Modules
import { baseUrl } from '../config/global.config';


const apiClient = request(`${baseUrl.replace(/\/?$/, '')}/faq-service/api/v1/faqs`);

export default function () {
    let _id;

    let newItem = {
        question: 'superTest?',
        answer: 'superTest!',
        status: 'draft'
    }
    let patchItem = {
        answer: 'superTESTUpdate',
        status: 'published'
    }

    it('GET Version', async function () {
        const response = await request(`${baseUrl.replace(/\/?$/, '')}/faq-service/version`).get('');

        addContext(this, { title: 'Version number', value: response.body.version });

        expect(response.body.version).to.not.be.undefined;
    });

    step('GET /faqs', async function () {
        const response = await list(this.token);

        expect(response.body).to.haveOwnProperty('count');
        expect(response.body).to.haveOwnProperty('skip');
        expect(response.body).to.haveOwnProperty('take');
        expect(response.body).to.haveOwnProperty('data');
        expect(response.body).to.haveOwnProperty('orderBy');
        expect(response.status).to.equal(200);
    });

    step('POST /faqs', async function () {
        const response = await postItem(newItem, this.token);

        expect(response.body).to.haveOwnProperty('_id');
        expect(response.status).to.equal(201);

        addContext(this, { title: 'New FAQ', value: response.body });

        _id = response.body._id;
    });

    step('GET /faqs/:id', async function () {
        const response = await getItem(_id, this.token);
        expect(response.status).to.equal(200);
    });

    step('PUT /faqs/:id', async function () {
        const response = await putItem(_id, patchItem, this.token);

        expect(response.status).to.equal(200);

        addContext(this, { title: 'Updated FAQ', value: response.body });
    });

    step('DELETE /faqs/:id', async function () {
        const response = await deleteItem(_id, this.token);
        expect(response.status).to.equal(204);
    });

};



export async function list(token: string) {
    return apiClient.get(``)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}

export async function getItem(id: string, token: string) {
    return apiClient.get(`/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}

export async function postItem(data: any, token: string) {
    return apiClient.post(``)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .send(data)
        .then((res) => {
            return res;
        });
}

export async function putItem(id: string, data: any, token: string) {
    return apiClient.put(`/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .send(data)
        .then((res) => {
            return res;
        });
}

export async function deleteItem(id: string, token: string) {
    return apiClient.delete(`/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}