// Public Packages
import { config } from 'dotenv';

// Private Packages
// None

// Internal Modules
// None

if (process.env.NODE_ENV === 'development') {
    console.log('Detected NODE_ENV === "development"; loading vars from .env file');
    config();
} else {
    console.log('Detected NODE_ENV !== "development"');
}


export const baseUrl = process.env.BASE_URL;
export const keycloakUrl = process.env.KEYCLOAK_URL;
export const keycloakRealm = process.env.KEYCLOAK_REALM;
export const keycloakClientId = process.env.KEYCLOAK_CLIENT_ID;
export const keycloakGrantType = process.env.KEYCLOAK_GRANT_TYPE || 'password';
export const keycloakClientSecret = process.env.KEYCLOAK_CLIENT_SECRET;
export const keycloakUsername = process.env.KEYCLOAK_USERNAME;
export const keycloakPassword = process.env.KEYCLOAK_PASSWORD;