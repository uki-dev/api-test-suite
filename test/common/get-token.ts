// Public Packages
import * as request from 'supertest';

// Private Packages
// None

// Internal Modules
import {
    keycloakClientId,
    keycloakClientSecret,
    keycloakGrantType,
    keycloakPassword,
    keycloakRealm,
    keycloakUrl,
    keycloakUsername
} from '../config/global.config';


export async function getToken() {
    let payload: any = {
        client_id: keycloakClientId,
        grant_type: keycloakGrantType,
        username: keycloakUsername,
        password: keycloakPassword
    };
    if (keycloakClientSecret != null) payload.client_secret = keycloakClientSecret;

    const response = await request(keycloakUrl)
        .post('/auth/realms/' + keycloakRealm + '/protocol/openid-connect/token')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept', '*/*')
        .set('Accept-Encoding', 'gzip, deflate, br')
        .send(payload);

    return response.body.access_token;
};