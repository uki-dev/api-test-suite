// Public Packages
import { expect } from 'chai';
import { step } from 'mocha-steps';
import * as request from 'supertest'
import * as addContext from 'mochawesome/addContext';

// Private Packages
// None

// Internal Modules
import { baseUrl } from '../config/global.config';


const apiClient = request(`${baseUrl.replace(/\/?$/, '')}/organizations-service/api/v1`);

export default function () {
    let _id;

    let newItem = {
        name: 'Test Org',
        shortName: 'TO',
        orgpath: 'Test/Org/Path'
    }
    let updatedItem = {
        shortName: 'TOrg'
    }
    let orgs = [];

    it('GET Version', async function () {
        const response = await request(`${baseUrl.replace(/\/?$/, '')}/organizations-service/version`).get('');

        addContext(this, { title: 'Version number', value: response.body.version });

        expect(response.body.version).to.not.be.undefined;
    });

    step('GET /organizations', async function () {
        const response = await list(this.token);

        expect(response.body).to.haveOwnProperty('count');
        expect(response.body).to.haveOwnProperty('skip');
        expect(response.body).to.haveOwnProperty('take');
        expect(response.body).to.haveOwnProperty('data');
        expect(response.body).to.haveOwnProperty('orderBy');
        expect(response.status).to.equal(200);

        orgs = response.body?.data || [];
        addContext(this, { title: 'Orgs', value: response.body });
    });

    step('POST /organizations', async function () {
        const response = await postItem(newItem, this.token);

        expect(response.body).to.haveOwnProperty('_id');
        expect(response.status).to.equal(200);

        addContext(this, { title: 'New Organization', value: response.body });
        _id = response.body._id;
    });

    step('GET /organizations/:id', async function () {
        // if (orgs.length > 0) {
        const response = await getItem(_id, this.token);
        expect(response.status).to.equal(200);
        // } else {
        //     this.skip();
        // }
    });

    step('GET /organizationsByPath/:path', async function () {
        // if (orgs.length > 0) {
        const response = await getItemByPath(newItem.orgpath, this.token);
        expect(response.status).to.equal(200);
        // } else {
        //     this.skip();
        // }
    });

    step('PATCH /organizations/:id', async function () {
        // if (orgs.length > 0) {
        const response = await patchItem(_id, updatedItem, this.token);

        expect(response.status).to.equal(200);

        addContext(this, { title: 'Updated organization', value: response.body });
        // } else {
        //     this.skip();
        // }
    });

    step('DELETE /organizations/:id', async function () {
        // if (orgs.length > 0) {
        const response = await deleteItem(_id, this.token);
        expect(response.status).to.equal(204);
        // } else {
        //     this.skip();
        // }
    });

};



export async function list(token: string) {
    return apiClient.get(`/organizations?$skip=0&$take=3`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}

export async function getItem(id: string, token: string) {
    return apiClient.get(`/organizations/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}

export async function getItemByPath(path: string, token: string) {
    return apiClient.get(`/organizationsByPath/${encodeURIComponent(path)}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}

export async function postItem(data: any, token: string) {
    return apiClient.post(`/organizations`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .send(data)
        .then((res) => {
            return res;
        });
}

export async function patchItem(id: string, data: any, token: string) {
    return apiClient.patch(`/organizations/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .send(data)
        .then((res) => {
            return res;
        });
}

export async function deleteItem(id: string, token: string) {
    return apiClient.delete(`/organizations/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}