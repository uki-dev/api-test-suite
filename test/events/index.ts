// Public Packages
import { expect } from 'chai';
import { step } from 'mocha-steps';
import * as request from 'supertest'
import * as addContext from 'mochawesome/addContext';

// Private Packages
// None

// Internal Modules
import { baseUrl } from '../config/global.config';


const apiClient = request(`${baseUrl.replace(/\/?$/, '')}/events-service/api/v1/events`);

export default function () {
    let _id;

    let newItem = {
        name: 'Automated Test Event',
        eventId: 'this-is-an-automated-test'
    }
    let patchItem = {
        name: 'Automated Test Event - MODIFIED',
    }

    it('GET Version', async function () {
        const response = await request(`${baseUrl.replace(/\/?$/, '')}/events-service/version`).get('');

        addContext(this, { title: 'Version number', value: response.body.version });

        expect(response.body.version).to.not.be.undefined;
    });

    step('GET /events', async function () {
        const response = await list(this.token);
        expect(response.status).to.equal(200);
    });

    step('POST /events', async function () {
        const response = await post(newItem, this.token);

        expect(response.body).to.haveOwnProperty('_id');
        expect(response.status).to.equal(201);

        addContext(this, { title: 'New FAQ', value: response.body });

        _id = response.body._id;
    });

    step('GET /events/:id', async function () {
        const response = await get(_id, this.token);
        expect(response.status).to.equal(200);
    });

    step('PUT /events/:id', async function () {
        const response = await put(_id, patchItem, this.token);

        expect(response.status).to.equal(200);
        expect(response.body.name).to.equal(patchItem.name);

        addContext(this, { title: 'Updated FAQ', value: response.body });
    });

    step('DELETE /events/:id', async function () {
        const response = await remove(_id, this.token);
        expect(response.status).to.equal(204);
    });

};



export async function list(token: string) {
    return apiClient.get(``)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}

export async function get(id: string, token: string) {
    return apiClient.get(`/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}

export async function post(data: any, token: string) {
    return apiClient.post(``)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .send(data)
        .then((res) => {
            return res;
        });
}

export async function put(id: string, data: any, token: string) {
    return apiClient.put(`/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .send(data)
        .then((res) => {
            return res;
        });
}

export async function remove(id: string, token: string) {
    return apiClient.delete(`/${id}`)
        .set('Authorization', 'bearer ' + token)
        .set('Accept', 'application/json')
        .then((res) => {
            return res;
        });
}