// Public Packages
// None

// Private Packages
// None

// Internal Modules
import { getToken } from './common/get-token';
import * as testplan from '../testplan/tests.json';


/**
 * Registers a test suite to be executed based upon configured parameters
 * @param slug filesystem-safe string describing a test suite and corresponding to a subdirectory containing the associated test
 * @param plan test plan settings
 */
const configureSuite = async (slug: string, plan: { name: string, enabled: boolean }) => {
    console.log(`ApiTestSuite.configureSuite :: Configuring suite [${slug}]`);

    // set up the test suite title and include the slug for reference
    const testName = `${plan.name || ''} [${slug}]`;

    // test folders must include an index.ts|js file which exports a default function
    let { default: testFn } = await import(`./${slug}`);

    // make sure the test is set up properly
    if (testFn == null) {
        testFn = function () { };
    }

    const suiteFn = function () {
        // this is required to prevent issues with 'mocha-steps' marking passed tests as "pending: true" if later steps fail
        // reference: https://github.com/rprieto/mocha-steps/issues/34
        this.retries(0);

        // get a new JWT before each suite
        before(async function () {
            this.token = await getToken();
        });

        // execute the test function
        testFn();
    }

    // use 'describe' if the test is enabled, otherwise use 'describe.skip' to still show the skipped test for documentation
    let suite = plan.enabled ? describe : describe.skip;

    // pass the test suite to Mocha
    suite(testName, suiteFn);
}

// each key of the 'testplan' JSON document is a 'slug' referencing a test folder
for (let slug in testplan) {
    configureSuite(slug, testplan[slug])
        .then(() => { })
        .catch(err => {
            if (err?.message?.includes('Cannot find module')) {
                console.warn(`ApiTestSuite.configureSuite :: Failed to import tests associated with ${slug}; please verify the path [${__dirname}/${slug}] exists and includes an index.ts|js file`);
            } else {
                console.error(err);
            }
        });
}