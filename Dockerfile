FROM ukidev/od360-nginx:dev-latest as prod

# Copy the compiled app into the container
COPY ./artifacts /usr/share/nginx/html
COPY ./artifacts/test-results.html /usr/share/nginx/html/index.html

# Copy nginx configs into the container
COPY ./nginx.conf /etc/nginx/nginx.conf